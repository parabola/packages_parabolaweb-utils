PACKAGE = parabolaweb-utils
VERSION = 20170205

# override gnuconf defaults
prefix      = /usr
sbindir     = $(bindir)
libexecdir  = $(exec_prefix)/lib
sysconfdir  = /etc/conf.d

# non-standard directories
systemddir    = $(prefix)/lib/systemd/system
uwsgidir      = /etc/uwsgi
pkglibexecdir = $(libexecdir)/parabolaweb-utils
pkgconffile   = $(sysconfdir)/parabolaweb

webuser = parabolaweb

CFLAGS += -std=c99 -Wall -Wextra -Werror -Wno-unused-parameter
CPPFLAGS += -DSCRIPT_LOCATION='"$(pkglibexecdir)/parabolaweb-changepassword.real"'

topoutdir ?= .
topsrcdir ?= .
include $(topsrcdir)/build-aux/Makefile.head.mk

targets = \
	$(sbindir)/parabolaweb-changepassword \
	$(sbindir)/parabolaweb-reporead-inotify \
	$(sbindir)/parabolaweb-reporead-rsync \
	$(sbindir)/parabolaweb-update \
	$(pkglibexecdir)/parabolaweb-changepassword.real \
	$(systemddir)/parabolaweb-reporead-inotify.service \
	$(systemddir)/parabolaweb-reporead-rsync.service \
	$(systemddir)/parabolaweb-reporead-rsync.timer \
	$(uwsgidir)/parabolaweb.ini

files.out.all = depends.txt $(filter-out $(files.src.src),$(notdir $(targets)))
files.sys.all = $(targets) $(pkgconffile)

# Pattern rules

%: %.in .var.sbindir .var.pkgconffile .var.webuser
	sed $(foreach v,$(patsubst .var.%,%,$(filter .var.%,$^)), -e 's|@$v@|$($v)|' ) < $< > $@

%: %.c .var.LINK.c
	$(LINK.c) $(filter-out .var.%,$^) -o $@

$(DESTDIR)$(sbindir)/%: %
	install -Dm755 $< $@

$(DESTDIR)$(pkglibexecdir)/%: %
	install -Dm755 $< $@

$(DESTDIR)$(systemddir)/%.service: %.service
	install -Dm644 $< $@

$(DESTDIR)$(systemddir)/%.timer: %.timer
	install -Dm644 $< $@

$(DESTDIR)$(uwsgidir)/%: %
	install -Dm644 $< $@

# Specific rules

depends.txt: list-depends depends_static.txt
	./$< > $@

$(DESTDIR)$(sbindir)/parabolaweb-changepassword: parabolaweb-changepassword
	install -Dm4755 $< $@

$(DESTDIR)$(pkgconffile): parabolaweb.conf
	install -Dm644 $< $@

.DELETE_ON_ERROR:
.SECONDARY:
.SUFFIXES:

include $(topsrcdir)/build-aux/Makefile.tail.mk
