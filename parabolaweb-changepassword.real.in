#!/bin/bash -e

#   Copyright (c) 2014, 2017 Luke Shumaker <lukeshu@sbcglobal.net>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

export PATH

usage() {
	printf 'Usage: %s [USERNAME]\n' "${0##*/}"
	printf 'A username may only be specified if run as root or @webuser@.\n'
}

main() {
	. @pkgconffile@
	[[ -e "${WEBDIR}/manage.py" ]]

	local REAL_USER=$USER
	if ! { [[ $SUID_USER == root ]] || [[ $SUID_USER == @webuser@ ]]; }; then
		unset SUDO_USER SUDO_UID SUDO_GID SUDO_COMMAND
	fi

	# The use-cases I want to handle:
	#                                  REAL SUID SUDO | NAME_OF others?
	# user$ changepassword             root user -    | user    no
	# user$ sudo changepassword        root root user | user    yes
	# user$ sudo -u web changepassword root web  user | user    yes
	# web$ changepassword              root web  -    | web     yes
	# root# changepassword             root root -    | root    yes
	# user$ /lib/.../changepasswoed    user -    -    | user    no
	local NAME_OF=${SUDO_USER:-${SUID_USER:-$REAL_USER}}
	local PERM_OF=${SUID_USER:-$REAL_USER}

	local username
	if [[ $PERM_OF == root ]] || [[ $PERM_OF == @webuser@ ]]; then
		if [[ $# -gt 1 ]]; then
			usage >&2
			return 1
		fi
		username=${1:-$NAME_OF}
	else
		if [[ $# -gt 0 ]]; then
			usage >&2
			return 1
		fi
		username=$NAME_OF
	fi

	sudo -u @webuser@ python2 "${WEBDIR}/manage.py" changepassword "${username}"
}

main "$@"
